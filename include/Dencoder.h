//
// Created by dhuck on 2/21/20.
// Dencoder is a library to decode the output of an Encoder. This may be
// preferable to a library than other libraries if the Encoder may be behind
// a multiplexer or other logic chip. You may want to hold a number of encoders
// in an array while handling yr pins and/or interrupts separately. This library
// is for the dreamers. This library is created to be free for any use but with
// no Warranty blah blah blah...
//
#include <Arduino.h>
#ifndef DENCODER_H
#define DENCODER_H

#define DEFAULT_LOW 0
#define DEFAULT_HIGH 1024
#define DEFAULT_POSITION 0

class Dencoder {
private:
    int16_t position;
    int16_t low;
    int16_t high;
    uint8_t pins[2];

public:
    Dencoder();
    Dencoder(int16_t lower, int16_t upper);
    Dencoder(int16_t lower, int16_t upper, int16_t start);

    int16_t read(uint8_t pinA, uint8_t pinB) volatile;
    void write(int16_t position);
    void setBoundaries(int16_t low, int16_t high);
};
#endif //DENCODER_H
