#ifndef UNIVERSE_H
#define UNIVERSE_H

#include "dmx_ch.h"
#include "MIDI.h"
#include "DmxSimple.h"
#include "fixture.h"

const int MAX_DMX_FXT = 16;
const int FXT_DATA_FIELDS = 3;
const int MAX_DMX_CH = 8;

// data locations for fixture array
const int FXT_FLAG = 0;
const int FXT_START_CHAN = 1;
const int FXT_NUM_CHAN = 2;


class Universe {
private:
    Fixture light[MAX_DMX_FXT];
    int chan;

public:
    //constructors
    Universe(int ch);

    // accessors

    // mutators

    // mgmt (future shit)
    void parse_cc(int cc, int val);
    void note_parse(int ch, int note, int vel);

    void spawn_fixture(int d_chans);

};

#endif /* end of include guard:  UNIVERSE_H */
