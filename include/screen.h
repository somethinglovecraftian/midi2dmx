#ifndef SCREEN_H_
#define SCREEN_H_

#ifndef SWITCHES
    #define SWITCHES 8
#endif

#ifndef ENCODERS
    #define ENCODERS 1
#endif

#include "Arduino.h"
#include "Wire.h"
#include "Adafruit_SSD1306.h"
#include "Adafruit_GFX.h"
#include <string>
#include "scene.h"
#include <array>

#define OLED_SCREEN_HEIGHT  64
#define OLED_SCREEN_WIDTH   128

// OLED display TWI address
#define OLED_ADDR 0x3C

// make sure library is correct

class Screen {
private:
    Adafruit_SSD1306 oled;
    // IntervalTimer displayTimer;

    // bad form
    int clock;
    char title[22] = "Disco Road           ";
    Scene scene;
public:
    Screen();
    void displayHeader(char head[]);
    void displayMidiNote(byte data1, byte data2);
    void displayCC(byte data1, byte data2);
    void clearLine(int x, int y, int w, int h);
    void displayClock();
    void writeDisplay();
    void setDownBeat();

    // development
    void displayButtons(std::array<volatile uint32_t, SWITCHES> buttons);
    void displayEncoders(std::array<volatile uint32_t, ENCODERS> encoders);


    char getTitle();
};

#endif // SCREEN_H_
