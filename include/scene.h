#ifndef SCENE_H_
#define SCENE_H_
#include "Arduino.h"

// sets the y-coordinate for each row of text
#define HEADER 0
#define ROW_1 9
#define ROW_2 18
#define ROW_3 27
#define ROW_4 36
#define ROW_5 45
#define FOOTER 54
#define MAX_ROWS 5

// CURRENTLY FOR TESTING
enum Row {
        ONE   = 9,
        TWO   = 18,
        THREE = 27,
        FOUR  = 36,
        FIVE  = 45
};

class Scene {
private:
    char *header;
    char *rows[5];
    char *footer[];

public:
    Scene();
    void setRow(int i);
    void setHeader(char head[]);
    void setFooter(char foot[]);
    void drawScene();
};

#endif // SCENE_H_
