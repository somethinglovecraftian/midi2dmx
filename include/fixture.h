#ifndef FIXTURE_H
#define FIXTURE_H

#include "Arduino.h"
#include "DmxSimple.h"
#include "dmx_ch.h"

const int CH = 8;

class Fixture {
private:
    Dmx_ch chans[CH];

public:
    // constructors
    Fixture();
    Fixture(int i);

    // accessors
    int get_chan(int i);

    // mutators
    void write(int i, int lvl);

    // mgmt functions
};

#endif /* end of include guard: FIXTURE_H */
