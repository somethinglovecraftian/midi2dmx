#ifndef DMX_CHANNEL_H
#define DMX_CHANNEL_H

#include "Arduino.h"
#include "DmxSimple.h"

class Dmx_ch {
private:
    int value;
    int d_chan;
public:
    // constructor
    Dmx_ch();

    // accessors
    int get_val();
    int get_chan();

    // mutators
    void set_val(int val);
    void set_chan(int ch);

    // actionable
    void write();
};
#endif /* end of include guard: DMX_CHANNEL_H */
