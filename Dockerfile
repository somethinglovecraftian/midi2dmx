## to Use:
# First build the docker image:
#
#       docker build -t midi2dmx .
#
# the first build will take awhile to download the base parts from the could
# when it is finished building, you can use the following command to build a hex
# file:
#
#       docker run -v $PWD:/opt/midi2dmx midi2dmx make
#
# Happy coding :)
#
# dhuck

FROM registry.gitlab.com/somethinglovecraftian/teensy_builder:latest

ENV PROJ_NAME=midi2dmx
ENV WORK_DIR=/opt/$PROJ_NAME

WORKDIR $WORK_DIR
