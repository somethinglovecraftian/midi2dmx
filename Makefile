# TODO: CPU speed configurable by teensy
# TODO: ignore main.cpp from cores
# Configurable options can be found in this file. This is where
# the teensy version and ARDUINO constant can be set for libraries and
# the needs of the board being compiled.
include mk/config.mk

# This file sets up all the flags and other variables to be used in the
# targets file. This is where the Sources and Libraries Locations and
# files are declared.
include mk/flags.mk

# This is where the make targets are designed and executed.
include mk/targets.mk

# Miscellaneous things are found here. There is a help target, which
# displays all the possible targets. A debug target that shows all of the
# build environment variables, and make targets to be used with sphinx
# to build documentation.
include $(wildcard mk/misc/*.mk)
