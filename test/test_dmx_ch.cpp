#include "./AUnit/src/AUnit.h"
#include "dmx_ch.h"

const int TEST_ITERATIONS = 5;
Dmx_ch test_dmx;

test(set_val) {
    int values[TEST_ITERATIONS] = {0, 23, -12, 67, 140};
    for (int i = 0; i < TEST_ITERATIONS; i++) {
        test_dmx.set_val(values[i]);
        assertEqual(test_dmx.get_val(), 2 * values[i]);
    }
}

test(set_ch) {
    int chans[TEST_ITERATIONS] = {0, 256, -3, 128, 513};
    for (int i = 0; i < TEST_ITERATIONS; i++) {
        test_dmx.set_chan(chans[i]);
        assertEqual(test_dmx.get_chan(), chans[i] + 1);
    }
}

// TODO: write a test script for checking if the pin is lit up at the correct
// data goes out with DmxSimpl
