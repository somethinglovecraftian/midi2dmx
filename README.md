# MIDI 2 DMX (Disco Road)

[![pipeline status](https://gitlab.com/somethinglovecraftian/midi2dmx/badges/dev/pipeline.svg)](https://gitlab.com/somethinglovecraftian/midi2dmx/commits/dev)

To celebrate DMX finally being released from prison and putting out new music,
we decided to make a simple device that converts incoming MIDI messages into
DMX messages that can be used to control lighting. Create your own light show
from Ableton or your favorite MIDI sequencer.

This project is still in its early stages. See below for planned features.

### Docker

Build the docker container locally on your machine and mount the
repository inside of the container using a volume. Run whichever make command
you wish and build the file. Running the following commands will produce a hex
file in your repository. Note: the first time you run this, it will need to
download the images to your harddrive.

```
make docker
```

To install the hex file on the teensy, you must have
[teensy-loader-cli](https://www.pjrc.com/teensy/loader_cli.html) installed on
your system. To upload the file, run:

```
make upload
```

### Windows

The easiest way to compile and upload the file on Windows is still by using
Platformio.

### Mac

Download and install the Arduino and Teensyduino tools and install them. Use
`make` to build and the Teensyduino loader to move the program over. For all
`make` commands run `make help`.

## Planned Features

### Firmware

-   Full Saturation of the DMX 512 Channel Universe
-   Direct CC control of DMX lights
-   Macros to control lights with Notes and CCs
-   Envelopes for Macros for gradual changes over time
-   One-Shot and Looping Envelopes
-   USB-Midi and DIN Midi
-   Midi Clock Sync

### Hardware

-   MIDI DIN input and DMX DIN output
-   Running on Teensy MCU
-   LCD display for editing
-   Knobby interface for direct editing
