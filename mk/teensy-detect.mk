
ifeq ($(TEENSY), 30)
    CPPFLAGS += -D__MK20DX128__ -mcpu=cortex-m4 -I$(COREPATH3)
    LDSCRIPT = $(COREPATH3)/mk20dx128.ld
    LDFLAGS += -mcpu=cortex-m4 -T$(LDSCRIPT)
	LOADFLAGS += --mcu=mkdx128
	TC_FILES := $(wildcard $(COREPATH3)/*.c)
	TCPP_FILES := $(wildcard $(COREPATH3)/*.cpp)
else ifeq ($(TEENSY), 31)
    CPPFLAGS += -D__MK20DX256__ -mcpu=cortex-m4 -I$(COREPATH3)
    LDSCRIPT = $(COREPATH3)/mk20dx256.ld
    LDFLAGS += -mcpu=cortex-m4 -T$(LDSCRIPT)
	LOADFLAGS += --mcu=mkdx256
	TC_FILES := $(wildcard $(COREPATH3)/*.c)
	TCPP_FILES := $(wildcard $(COREPATH3)/*.cpp)
else ifeq ($(TEENSY), LC)
    CPPFLAGS += -D__MKL26Z64__ -mcpu=cortex-m0plus -I$(COREPATH3)
    LDSCRIPT = $(COREPATH3)/mkl26z64.ld
    LDFLAGS += -mcpu=cortex-m0plus -T$(LDSCRIPT)
	LOADFLAGS += --mcu=mkl26z64
	TC_FILES := $(wildcard $(COREPATH3)/*.c)
	TCPP_FILES := $(wildcard $(COREPATH3)/*.cpp)
    LIBS += -larm_cortexM0l_math
else ifeq ($(TEENSY), 35)
    CPPFLAGS += -D__MK64FX512__ -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -I$(COREPATH3)
    LDSCRIPT = $(COREPATH3)/mk64fx512.ld
    LDFLAGS += -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -T$(LDSCRIPT)
	LDFLAGS += -mthumb -fsingle-precision-constant
	LOADFLAGS += --mcu=mk64fx512
	TC_FILES := $(wildcard $(COREPATH3)/*.c)
	TCPP_FILES := $(wildcard $(COREPATH3)/*.cpp)
    LIBS += -larm_cortexM4lf_math
else ifeq ($(TEENSY), 36)
    CPPFLAGS += -D__MK66FX1M0__ -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -I$(COREPATH3)
    LDSCRIPT = $(COREPATH3)/mk66fx1m0.ld
    LDFLAGS += -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -T$(LDSCRIPT)
	LOADFLAGS += --mcu=mk66fx1m0
	TC_FILES := $(wildcard $(COREPATH3)/*.c)
	TCPP_FILES := $(wildcard $(COREPATH3)/*.cpp)
    LIBS += -larm_cortexM4lf_math
else ifeq ($(TEENSY), 40)
	CPPFLAGS += -D__IMXRT1062__ -mcpu=cortex-m7 -mfloat-abi=hard -mfpu=fpv5-d16 -I$(COREPATH4)
	CPPFLAGS += -DARDUINO_TEENSY40
	LDSCRIPT = $(COREPATH4)/imxrt1062.ld
	LDFLAGS += -T$(abspath $(LDSCRIPT)) -mcpu=cortex-m7 -mfloat-abi=hard -mfpu=fpv5-d16
	# LDFLAGS += -fsingle-precision-constant -nostdlib -nostartfiles
	LOADFLAGS += --mcu=imxrt1062
	TC_FILES := $(wildcard $(COREPATH4)/*.c)
	TCPP_FILES := $(wildcard $(COREPATH4)/*.cpp)
	LIBS += -larm_cortexM7lfsp_math
else
    $(error Invalid setting for TEENSY)
endif

# Make sure the main file from the teensy cores is not included in the files
TCPP_FILES := $(filter-out $(COREPATH4)/main.cpp, $(TCPP_FILES))
TCPP_FILES := $(filter-out $(COREPATH3)/main.cpp, $(TCPP_FILES))
