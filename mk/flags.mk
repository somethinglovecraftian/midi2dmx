# CPPFLAGS = compiler options for C and C++
CPPFLAGS = -O2 -g -mthumb -ffunction-sections -fdata-sections -nostdlib -DTEENSYVER=$(TEENSY)
CPPFLAGS += -MMD $(OPTIONS) -DTEENSYDUINO=$(TEENSYDUINO) -DF_CPU=$(TEENSY_CORE_SPEED)
CPPFLAGS += -Isrc -I$(INCLUDE) -Wno-deprecated-declarations
# Uncomment for warnings:
# CPPFLAGS += -Wall


# compiler options for C++ only
CXXFLAGS = -felide-constructors -fno-exceptions -fno-rtti -fpermissive
CXXFLAGS += -fno-threadsafe-statics -std=gnu++14 -Wno-error=narrowing
# compiler options for C only
CFLAGS =

# linker options
LDFLAGS = -O2 -Wl,--gc-sections,--relax,--defsym=__rtc_localtime=$(shell date +%s)
LDFLAGS += -mthumb

# additional libraries to link
LIBS = -lm

include mk/teensy-detect.mk

# set arduino define if given
ifdef ARDUINO
	CPPFLAGS += -DARDUINO=$(ARDUINO)
else
	CPPFLAGS += -DUSING_MAKEFILE
endif

# names for the compiler programs
CC = $(abspath $(COMPILERPATH))/arm-none-eabi-gcc
CXX = $(abspath $(COMPILERPATH))/arm-none-eabi-g++
OBJCOPY = $(abspath $(COMPILERPATH))/arm-none-eabi-objcopy
OBJDUMP = $(abspath $(COMPILERPATH))/arm-none-eabi-objdump
SIZE = $(abspath $(COMPILERPATH))/arm-none-eabi-size
AR = $(abspath $(COMPILERPATH))/arm-none-eabi-gcc-ar


# automatically create lists of the sources and objects
LC_FILES := $(shell python mk/py/pyfind.py lib .c)
LCPP_FILES := $(shell python mk/py/pyfind.py lib .cpp)
C_FILES := $(wildcard src/*.c)
CPP_FILES := $(wildcard src/*.cpp)
INO_FILES := $(wildcard src/*.ino)

# TODO: modify pyfind.py to allow for pyfind searching inside of AUnit
TSTCPP_FILES := $(shell python mk/py/pytest.py test .cpp) $(shell python mk/py/pytest.py src .cpp)

# include paths for libraries
L_INC := $(foreach lib, $(shell python mk/py/pyfind.py lib .h),-I$(lib))
# L_INC := $(shell find $(LIBRARYPATH) -name '*.h')

SOURCES := $(TC_FILES:.c=.o) $(TCPP_FILES:.cpp=.o) $(LC_FILES:.c=.o) $(LCPP_FILES:.cpp=.o) $(C_FILES:.c=.o) $(CPP_FILES:.cpp=.o) $(INO_FILES:.ino=.o)
OBJS := $(foreach src,$(SOURCES),$(BUILDDIR)/$(src))

TST_SOURCES := $(TSTCPP_FILES:.cpp=.o) $(INO_FILES:.ino=.o) $(TC_FILES:.c=.o) $(TCPP_FILES:.cpp=.o) $(LC_FILES:.c=.o) $(LCPP_FILES:.cpp=.o)
TST_OBJS := $(foreach src,$(TST_SOURCES),$(BUILDDIR)/$(src))
