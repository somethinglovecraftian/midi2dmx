all: hex ## default: Builds the HEX file

build: $(TARGET).elf ## build only the ELF file

hex: $(TARGET).hex ## build HEX from existing ELF file

docker:  ## compile hex from docker image
	@python mk/py/pydocker.py $(TARGET) make

upload: $(TARGET).hex ## do upload and reboot
	@teensy_loader_cli -vw $(LOADFLAGS) $(TARGET).hex

monitor: upload ## upload and start serial monitor
	@echo "Waiting for device to finish rebooting..."
	@sleep 2.5
	@pio device monitor

test: $(TSTTARG).elf $(TSTTARG).hex
	@teensy_loader_cli -vw --mcu=mk64fx512 $(TSTTARG).hex
	@echo "Waiting for device to finish rebooting..."
	@sleep 2.5
	@pio device monitor


$(BUILDDIR)/%.o: %.c
	@echo -e "[CC ]\t$<"
	@mkdir -p "$(dir $@)"
	@$(CC) $(CPPFLAGS) $(CFLAGS) $(L_INC) -o "$@" -c "$<"

$(BUILDDIR)/%.o: %.cpp
	@echo -e "[CXX]\t$<"
	@mkdir -p "$(dir $@)"
	@$(CXX) $(CPPFLAGS) $(CXXFLAGS) $(L_INC) -o "$@" -c "$<"

$(BUILDDIR)/%.o: %.ino
	@echo -e "[CXX]\t$<"
	@mkdir -p "$(dir $@)"
	@$(CXX) $(CPPFLAGS) $(CXXFLAGS) $(L_INC) -o "$@" -x c++ -include Arduino.h -c "$<"

$(TARGET).elf: $(OBJS) $(LDSCRIPT)
	@echo -e "[LD ]\t$@"
	@$(CC) $(LDFLAGS) -o "$@" $(OBJS) $(LIBS)

$(TSTTARG).elf: $(TST_OBJS) $(LDSCRIPT)
	@echo -e "[LD ]\t$@"
	@$(CC) $(LDFLAGS) -o "$@" $(TST_OBJS) $(LIBS)

%.hex: %.elf
	@echo -e "[HEX]\t$@"
	@$(SIZE) "$<"
	@$(OBJCOPY) -O ihex -R .eeprom "$<" "$@"

# compiler generated dependency info
-include $(OBJS:.o=.d)

clean:
	@echo Cleaning...
	rm -rf "$(BUILDDIR)"
	rm -f "$(TARGET).elf" "$(TARGET).hex"
