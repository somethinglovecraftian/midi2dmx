# Makefile help system

.PHONY: help
help:	## display help messages
	@python mk/py/pyhelp.py$(MAKEFILE_LIST)
