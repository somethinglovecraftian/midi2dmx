from os import popen
from os import system as sh
from sys import argv

def docker(image, command):
    images = popen('docker images --format "{{.Repository}}"').read().split('\n')

    # check if the docker container already exists
    try:
        if image not in images:
            print('[DOCKER] Building image...')
            sh('docker build -t {} .'.format(image))

        print('[DOCKER] Running make inside of docker...')
        sh('docker run -v $PWD:/opt/{} {} {}'.format(image, image, command))
        return 0
    except:
        print('Docker commands failed! Is docker installed on your system?')
        return -1
docker(argv[1], " ".join(argv[2:]))
