import os
import sys

def exclude(path):

    # add keywords depending on different libraries here
    # everything in this list will be ignored
    excludes = ['tests', 'doc', 'example', 'build', 'external', 'extra', 'rst',
                'contrib', 'fake', 'duino']
    for ex in excludes:
        if ex in path:
            return False
            break

    return True

dir = sys.argv[1]
ext = sys.argv[2]
result = []

# header files first (searches for parent path)
if ext == '.h' or ext == '.hpp':
    for root, dirs, files in os.walk(dir):
        for name in files:
            if name.endswith(ext):
                path = os.path.join(root)
                if path not in result and exclude(path):
                    result.append(os.path.join(root))

else:
    for root, dirs, files in os.walk(dir):
        for name in files:
            if name.endswith(ext) and name != "main.cpp":
                path = os.path.join(root)
                if exclude(path):
                    result.append(os.path.join(root,name))
print(" ".join(result));
