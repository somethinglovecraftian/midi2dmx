# The teensy version to use, 30, 31, 35, 36, or LC
TEENSY = 40
TEENSYDUINO = 147

# Set to 24000000, 48000000, or 96000000 to set CPU core speed
# TEENSY_CORE_SPEED = 120000000  # Teensy 3.5
TEENSY_CORE_SPEED = 600000000

# The name of your project (used to name the compiled .hex file)
TARGET = $(notdir $(CURDIR))
TSTTARG = tst-$(notdir $(CURDIR))
# Some libraries will require this to be defined
# If you define this, you will break the default main.cpp
ARDUINO = 10808
ARDUINO_TEENSY40 = 10808

# configurable options
OPTIONS = -DUSB_SERIAL -DLAYOUT_US_ENGLISH

# directory to build in
BUILDDIR = $(abspath $(CURDIR)/build)

#************************************************************************
# Location of Teensyduino utilities, Toolchain, and Arduino Libraries.
# To use this makefile without Arduino, copy the resources from these
# locations and edit the pathnames.  The rest of Arduino is not needed.
#************************************************************************

include mk/os-detect.mk

# path location for Teensy 3 core
COREPATH3 = cores/teensy3

# path location for Teensy 4 core
COREPATH4 = cores/teensy4

# path location for Arduino libraries
LIBRARYPATH = lib

# include path for headers
INCLUDE = include

# path location for the arm-none-eabi compiler
COMPILERPATH = $(TOOLSPATH)/arm/bin
