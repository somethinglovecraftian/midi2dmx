#include <DmxSimple.h>

#define DMX_PIN 3
#define MAX_DMX_LIGHTS 1
#define DMX_CHANNELS_PER_LIGHT 8
#define BASE_MIDI_CHANNEL 1

struct light {
  byte brightness, red, green, blue, uv, strobe_type, strobe_speed, dmx_mode;
  bool changed;
};

light dmx_lights[MAX_DMX_LIGHTS];


static byte counter;

void setup() {
  // initialize serial:
  Serial.begin(9600);
  // reserve 200 bytes for the inputString:
  Serial.println("Setting up");
  //This is the DMX output pin.
  DmxSimple.usePin(DMX_PIN);
  dmx_clear();
  counter = 0;
  
}

light init_light(light l) {
  l.brightness = 0;
  l.red = 0;
  l.green = 0;
  l.blue = 0;
  l.uv = 0;
  l.strobe_type = 0;
  l.strobe_speed = 0;
  l.dmx_mode = 0;
  l.changed = false;
  return l;
}

void send_light(int dmx_light) {
  byte base = dmx_light * DMX_CHANNELS_PER_LIGHT;
  /*
  Serial.print("Sending light: ");
  Serial.println(dmx_light);
  Serial.print("Base channel: ");
  Serial.println(base +1);
  Serial.print("Brightness: ");
  Serial.println(dmx_lights[dmx_light].brightness);
  Serial.print("Red: ");
  Serial.println(dmx_lights[dmx_light].red);
  Serial.print("Green: ");
  Serial.println(dmx_lights[dmx_light].green);
  Serial.print("Blue: ");
  Serial.println(dmx_lights[dmx_light].blue);
  */
  // brightness, red, green, blue, uv, strobe_type, strobe_speed, dmx_mode
  DmxSimple.write(base + 8, dmx_lights[dmx_light].dmx_mode);
  DmxSimple.write(base + 7, dmx_lights[dmx_light].strobe_speed);
  DmxSimple.write(base + 6, dmx_lights[dmx_light].strobe_type);
  DmxSimple.write(base + 5, dmx_lights[dmx_light].uv);
  DmxSimple.write(base + 4, dmx_lights[dmx_light].blue);
  DmxSimple.write(base + 3, dmx_lights[dmx_light].green);
  DmxSimple.write(base + 2, dmx_lights[dmx_light].red);
  DmxSimple.write(base + 1, dmx_lights[dmx_light].brightness);
}

void dmx_clear() {
  for (byte x = 0; x < MAX_DMX_LIGHTS; x++) {
    dmx_lights[x] = init_light(dmx_lights[x]);
    send_light(x);
    Serial.print("Initializing light: ");
    Serial.println(x);
  }
}
void loop() {
  if (counter >= 32) {
    counter = 0;
  }
  
  if (counter % 2 == 0) {
    dmx_lights[0].brightness = 255;
  } else {
    dmx_lights[0].brightness = 0;
  }
  
  if (counter % 4 == 0) {
    dmx_lights[0].red = 255;
  } else {
    dmx_lights[0].red = 0;
  }
  
  if (counter % 8 == 0) {
    dmx_lights[0].green = 255;
  } else {
    dmx_lights[0].green = 0;
  }
  
  if (counter % 16 == 0) {
    dmx_lights[0].blue = 255;
  } else {
    dmx_lights[0].blue = 0;
  }

  send_light(0);
  counter++;
  
    Serial.print(counter);
    Serial.print("\n");
    delay(250);
}
