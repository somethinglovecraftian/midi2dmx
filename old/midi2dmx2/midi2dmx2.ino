#include <MIDI.h>
#include <DmxSimple.h>

#define DMX_PIN 3
#define MAX_DMX_LIGHTS 12
#define DMX_CHANNELS_PER_LIGHT 8
#define BASE_MIDI_CHANNEL 1

struct light {
  byte c1, c2, c3, c4, c5, c6, c7, c8;
  int changed;
};

light dmx_lights[MAX_DMX_LIGHTS];

MIDI_CREATE_DEFAULT_INSTANCE();

void setup() {
  //This is the DMX output pin.
  DmxSimple.usePin(DMX_PIN);
  MIDI.begin(MIDI_CHANNEL_OMNI);
  dmx_clear();
}

light init_light(light l) {
  l.c1 = 0;
  l.c2 = 0;
  l.c3 = 0;
  l.c4 = 0;
  l.c5 = 0;
  l.c6 = 0;
  l.c7 = 0;
  l.c8 = 0;
  l.changed = -1;
  return l;
}

void send_light(int dmx_light) {
  byte base = dmx_light * DMX_CHANNELS_PER_LIGHT;
  switch (dmx_lights[dmx_light].changed) {
    case 0:
      break;
    case 1:
      DmxSimple.write(base + 1, dmx_lights[dmx_light].c1);
      break;
    case 2:
      DmxSimple.write(base + 2, dmx_lights[dmx_light].c2);
      break;
    case 3:
      DmxSimple.write(base + 3, dmx_lights[dmx_light].c3);
      break;
    case 4:
      DmxSimple.write(base + 4, dmx_lights[dmx_light].c4);
      break;
    case 5:
      DmxSimple.write(base + 5, dmx_lights[dmx_light].c5);
      break;
    case 6:
      DmxSimple.write(base + 6, dmx_lights[dmx_light].c6);
      break;
    case 7:
      DmxSimple.write(base + 7, dmx_lights[dmx_light].c7);
      break;
    case 8:
      DmxSimple.write(base + 8, dmx_lights[dmx_light].c8);
      break;
    case -1:
      DmxSimple.write(base + 8, dmx_lights[dmx_light].c8);
      DmxSimple.write(base + 7, dmx_lights[dmx_light].c7);
      DmxSimple.write(base + 6, dmx_lights[dmx_light].c6);
      DmxSimple.write(base + 5, dmx_lights[dmx_light].c5);
      DmxSimple.write(base + 4, dmx_lights[dmx_light].c4);
      DmxSimple.write(base + 3, dmx_lights[dmx_light].c3);
      DmxSimple.write(base + 2, dmx_lights[dmx_light].c2);
      DmxSimple.write(base + 1, dmx_lights[dmx_light].c1);
      break;
  }
  dmx_lights[dmx_light].changed = 0;
}

void dmx_clear() {
  for (byte x = 0; x < MAX_DMX_LIGHTS; x++) {
    dmx_lights[x] = init_light(dmx_lights[x]);
    send_light(x);
  }
}

int process_midi() {
  int retval = -1;
  if (MIDI.read()) {
    // get the midi data
    byte midi_data_one = MIDI.getData1();
    byte midi_data_two = MIDI.getData2();
    byte midi_type = MIDI.getType();
    byte midi_chan = MIDI.getChannel();

    if (midi_chan == BASE_MIDI_CHANNEL) {
      if (midi_type == midi::NoteOn || midi_type == midi::NoteOff) {
        byte note = midi_data_one;
        byte velocity = midi_data_two;
        byte dmx_light = midi_data_one % MAX_DMX_LIGHTS;
        retval = dmx_light;
        if (note == 127) {
          dmx_clear();
        } else if (note < 126) {
          dmx_lights[dmx_light].c1 = velocity * 2;
          dmx_lights[dmx_light].changed = 1;
        }
      } else if (midi_type == midi::ControlChange) {
        byte cc = midi_data_one % DMX_CHANNELS_PER_LIGHT;
        byte val = midi_data_two;
        byte dmx_light = midi_data_one / DMX_CHANNELS_PER_LIGHT;
        retval = dmx_light;
        switch (cc) {
            case 0:
              dmx_lights[dmx_light].c1 = val * 2;
              dmx_lights[dmx_light].changed = 1;
              break;
            case 1:
              dmx_lights[dmx_light].c2 = val * 2;
              dmx_lights[dmx_light].changed = 2;
              break;
            case 2:
              dmx_lights[dmx_light].c3 = val * 2;
              dmx_lights[dmx_light].changed = 3;
              break;
            case 3:
              dmx_lights[dmx_light].c4 = val * 2;
              dmx_lights[dmx_light].changed = 4;
              break;
            case 4:
              dmx_lights[dmx_light].c5 = val * 2;
              dmx_lights[dmx_light].changed = 5;
              break;
            case 5:
              dmx_lights[dmx_light].c6 = val * 2;
              dmx_lights[dmx_light].changed = 6;
              break;
            case 6:
              dmx_lights[dmx_light].c7 = val * 2;
              dmx_lights[dmx_light].changed = 7;
              break;
            case 7:
              dmx_lights[dmx_light].c8 = val * 2;
              dmx_lights[dmx_light].changed = 8;
              break;
        }
      }
    }
  }
  return retval;
}

void loop() {
  static int l = process_midi();
  if (l > 0) {
    send_light(l);
  }
}
