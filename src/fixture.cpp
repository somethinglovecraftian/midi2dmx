#include "fixture.h"

Fixture::Fixture() {

}


int Fixture::get_chan(int i) {
    if (i < 8 ) {
        return chans[i].get_val();
    }
}

void Fixture::write(int i, int lvl){
    chans[i].set_val(lvl);
    chans[i].set_chan(i);
    chans[i].write();
}
