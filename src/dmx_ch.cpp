#include "dmx_ch.h"
#include <math.h>

// constructor

Dmx_ch::Dmx_ch() {

}
// accessors
int Dmx_ch::get_val() {
    return value;
}

int Dmx_ch::get_chan() {
    return d_chan;
}

// mutators
void Dmx_ch::set_val(int val) {
    // TODO: Write a better mapping function. Log to halfway and exp the rest?
    if (val == 0) {
        value = 0;
    }
    else {
        value = 256 / 127 * val;
    }
    Serial.print((String)"Value: " + value + "\n");

}

void Dmx_ch::set_chan(int ch) {
    if (ch > 512) {
        d_chan = 512;
    }
    d_chan = ch + 1;
    Serial.print((String)"Chan: " + ch + "\n");
}

// actionable
void Dmx_ch::write() {
    DmxSimple.write(d_chan, value);
}
