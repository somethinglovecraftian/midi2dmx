#include "screen.h"

Screen::Screen() : oled(OLED_SCREEN_WIDTH, OLED_SCREEN_HEIGHT, &Wire, -1) {
    oled.begin(SSD1306_SWITCHCAPVCC, OLED_ADDR);
    oled.setTextSize(1);
    oled.setTextColor(WHITE);
    oled.clearDisplay();
    displayHeader(title);
    clock = 0;
//    oled.setCursor(0,15);
//    oled.print("NOT :\n");
//    oled.print("VEL :\n");
//    oled.print("CC  :\n");
//    oled.print("VAL :\n");
    writeDisplay();

}
void Screen::writeDisplay() {
    oled.display();
}

void Screen::clearLine(int x, int y, int w=OLED_SCREEN_WIDTH - 1, int h=8) {
    if (w == OLED_SCREEN_WIDTH - 1) {
        w -= x; // clear the remainder of the row.
    }
    oled.fillRect(x, y, w, h, BLACK);
}

void Screen::displayHeader(char head[]) {
    // oled.clearDisplay();
    clearLine(0, 0);
    oled.setCursor(0,0);
    oled.print(head);
    oled.print("---------------------\n");
}

void Screen::displayMidiNote(byte data1, byte data2) {
    clearLine(32, 15, 3*8);
    oled.setCursor(32,15);
    oled.print(data1);
    clearLine(32,23, 3*8);
    oled.setCursor(32,23);
    oled.print(data2);
    oled.print("    ");
    // oled.display();
}

void Screen::displayCC(byte data1, byte data2) {
    clearLine(32,31, 3*8);
    oled.setCursor(32,31);
    oled.print(data1);
    oled.print("    ");
    clearLine(32,39, 3*8);
    oled.setCursor(32,39);
    oled.print(data2);
    oled.print("    ");
}

void Screen::setDownBeat() {
    clock = 0;
}

void Screen::displayClock() {
    if (clock % 24 == 0) {
        oled.drawPixel(127, 0, WHITE);
        clock = 0;
    }
    else if (clock % 12 == 0) {
        oled.drawPixel(127, 0, BLACK);
    }
    clock++;
}

// development functions. Will be removed for production
void Screen::displayButtons(std::array<volatile uint32_t, SWITCHES> buttons) {
    oled.clearDisplay();
    displayHeader(title);
    for (uint32_t i = 0; i < 4; i++) {
        oled.setCursor(0, 15 + (i * 8));
        oled.printf("BUTT %d:  %d BUTT %d:  %d",
                i, buttons[i], i + 4, buttons[i+4]);
    }
}

void Screen::displayEncoders(std::array<volatile uint32_t, ENCODERS> encoders) {
    for (uint32_t i = 0; i < 2; i++) {
        oled.setCursor(0, 47 + (i*8));
        oled.printf("EN%d: %0.4d EN%d: %0.4d",
                i, encoders[i], i + 2, encoders[i+2]);
    }
}
