//
// Created by dhuck on 2/21/20.
//
#include "Dencoder.h"

Dencoder::Dencoder() : position(DEFAULT_POSITION), low(DEFAULT_LOW),
                        high(DEFAULT_HIGH) {
    pins[0] = LOW;
    pins[1] = LOW;
}

Dencoder::Dencoder(int16_t lower, int16_t upper) : position(DEFAULT_POSITION),
                        low(lower), high(upper) {
    pins[0] = LOW;
    pins[1] = LOW;
}


Dencoder::Dencoder(int16_t lower, int16_t upper, int16_t start): position(start),
                        low(lower), high(upper) {
    pins[0] = LOW;
    pins[1] = LOW;
}

int16_t Dencoder::read(uint8_t pinA, uint8_t pinB) volatile {
    if (pins[0] != pinA) {
        if (pinA != pinB) {
            position++;
        }
        else {
            position--;
        }
    }
    pins[0] = pinA;
    return position;
}

void Dencoder::write(int16_t pos) {
    position = pos;
}
void Dencoder::setBoundaries(int16_t lower, int16_t upper) {
    low = lower;
    high = upper;
}