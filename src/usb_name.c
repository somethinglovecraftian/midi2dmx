#include "usb_names.h"

#define MIDI_NAME {'D', 'i', 's', 'c', 'o', ' ', 'R', 'o', 'a', 'd'}
#define MIDI_NAME_LEN 10

struct usb_string_descriptor_struct usb_string_product_name = {
        2 + MIDI_NAME_LEN * 2,
        3,
        MIDI_NAME
};
