// For Dev Purposes. Set to 1 to enable Serial writes below
#define DEV false
#define SERIAL_RATE 115200
// Other definitions
#define LED_PIN 13
#define PERIOD_1_KHZ 125
#define PERIOD_120_HZ 8333
#define PERIOD_90_HZ 11111
#define PERIOD_60_HZ 16667
#define PERIOD_30_HZ 33333
#define PERIOD_15_HZ 66666

#define SWITCHES 8
#define ENCODERS 1

#include "DmxSimple.h"
#include "MIDI.h"
#include "universe.h"
#include "Encoder.h"
//#include "MIDIUSB.h" // not currently supported in teensy4
#include "screen.h"
#include "FortyFiftyOne.h"
#include <array>
#include <IntervalTimer.h>
#include <Arduino.h>
#include "Dencoder.h"

// sets the base midi chan.
const int MIDICH = 1;

// DMX and MIDI, what we came here to do

auto *rig = new Universe(1);
//MIDI_CREATE_INSTANCE(HardwareSerial, Serial1, MIDI);

// Screen Timer
auto *screen = new Screen();



// Interface timer stuff
auto *switches = new FortyFiftyOne(8, INPUT_PULLDOWN, DIGITAL, 9, SWITCHES, UNDEFINED);
std::array<volatile Encoder, ENCODERS> dencoders {Encoder(6, 7)};
std::array<volatile uint32_t, SWITCHES> buttons({0});
std::array<volatile uint32_t, ENCODERS> encoders({0});

volatile uint32_t buttonCtr = 0;
volatile uint32_t encoderCtr = 0;

//for (int i = 0; i < ENCODERS; i++) {
//    dencoders[i] = new Encoder(3, 4)
//}

void updateInterface() {
    for (int i = 0; i < ENCODERS; i++) {
        int32_t val = dencoders[i].read() / 4;
        if (val != encoders[i]) {
            encoders[i] = val;
        }
    }
    buttons[buttonCtr] = switches->read(buttonCtr);
    buttonCtr = (buttonCtr + 1) % SWITCHES;
}

void displayHandler() {
//    Serial.println("Display Handler");
    std::array<volatile uint32_t, SWITCHES> buttonCpy({0});
    std::array<volatile uint32_t, ENCODERS> encoderCpy({0});
//    noInterrupts();
    encoderCpy = encoders;
    buttonCpy = buttons;
    screen->displayButtons(buttonCpy);
    screen->displayEncoders(encoderCpy);
//    interrupts();
    screen->writeDisplay();
}

void dmx_clear() {
    for (int i = 0; i < 512; i++) {
        DmxSimple.write(i, 0);
    }
}

// LED Boot animation for successful boot
void bootNotification() {
    int init_delay = 200;
    for (int i = 0; i < 4; i++)
    {
        init_delay = init_delay / 2;
        digitalWrite(LED_PIN, HIGH);
        delay(init_delay);
        digitalWrite(LED_PIN, LOW);
        delay(init_delay);
    }
    if (DEV) digitalWrite(LED_PIN, HIGH);
}

//void process_midi(byte ch, byte type, byte data1, byte data2) {
//    switch (type) {
//        case 0x90: // Note On
//            screen->displayMidiNote(data1, data2);
//            break;
//        case 0x80: // Note Off
//            break;
//        case 0xB0: // Control Change
//            rig->parse_cc(data1, data2);
//            screen->displayCC(data1, data2);
//            break;
//        case 0xA0: // Poly Aftertouch
//            break;
//        case 0xC0: // Program Change
//            break;
//        case 0xD0: // ch Aftertouch
//            break;
//        case 0xE0: // Pitchbend
//            break;
//        case 0xF0: // Sysex
//            break;
//        case 0xF1: // Time Code Quarter Frame (??)
//            break;
//        case 0xF2: // Song Position
//            break;
//        case 0xF3: // Song Select (??)
//            break;
//        case 0xF6: // Tune Request (??)
//            break;
//        case 0xF8: // Clock
//            screen->displayClock();
//            break;
//        case 0xFA: // Start
//            screen->setDownBeat();
//            break;
//        case 0xFB: // Continue
//            break;
//        case 0xFC: // Stop
//            screen->setDownBeat();
//            break;
//        case 0xFE: // ActiveSensing
//            break;
//        case 0xFF: // System Reset
//            break;
//    }
//}

void setup(IntervalTimer *displayTimer) {
//    MIDI.begin(0);
//    DmxSimple.usePin(53);
    pinMode(LED_PIN, OUTPUT);
    if (DEV) {
        Serial.begin(SERIAL_RATE);
        while (!Serial);
        dmx_clear();
        delay(1500);
        Serial.println("\n\n\n");
        Serial.println("DISCO ROAD now loaded");
        Serial.println("====================== \n\n");
        Serial.println("Starting Interval Timers...\n");
        displayTimer->begin(displayHandler, PERIOD_15_HZ);
//        displayTimer->priority(255);
        Serial.println("Started display timer");
//        buttonTimer->begin(updateInterface, (PERIOD_1_KHZ / SWITCHES));
//        buttonTimer->priority(63);
        Serial.println("Started buttonTimer");
    }
    else {
        displayTimer->begin(displayHandler, PERIOD_30_HZ);
        displayTimer->priority(255);
//        buttonTimer->begin(updateInterface, PERIOD_1_KHZ / SWITCHES);
//        buttonTimer->priority(63);
    }
    bootNotification();
}
#pragma clang diagnostic push
#pragma ide diagnostic ignored "EndlessLoop"
int main() {
    auto *displayTimer = new IntervalTimer();
//    auto *buttonTimer = new IntervalTimer();

    setup(displayTimer);
    if (DEV) {
        Serial.println("Made it back into main!");
    }

    for (;;) {
        updateInterface();
//        encoders[encoderCtr] = dencoders[encoderCtr].read();
        // byte type, ch, data1, data2;
        // if (MIDI.read()) {
        //     type = MIDI.getType();
        //     ch = MIDI.getChannel();
        //     data1 = MIDI.getData1();
        //     data2 = MIDI.getData2();
        // }
        // Serial.println("Looking for channel 1:");
        // process_midi(ch, type, data1, data2);
    }
    return 0;
}
#pragma clang diagnostic pop

void loop() {
//    Serial.println("lol");




    // if (usbMIDI.read()) {
    //     type = usbMIDI.getType();
    //     ch = usbMIDI.getChannel();
    //     data1 = usbMIDI.getData1();
    //     data2 = usbMIDI.getData2();
    // }

}
